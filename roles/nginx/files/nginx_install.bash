#!/bin/bash

curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor | sudo tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null

key_downloaded=$(gpg --dry-run --quiet --no-keyring --import --import-options import-show /usr/share/keyrings/nginx-archive-keyring.gpg)
key_expected="573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62"


# if fingerprint signature matches, then set up preference to nginx official packages over the distribution-provided ones and update package list
if [[ $key_downloaded == *$key_expected* ]]; then
  echo "===fingerprint approved==="
  echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] http://nginx.org/packages/ubuntu `lsb_release -cs` nginx" | sudo tee /etc/apt/sources.list.d/nginx.list

  echo -e "Package: *\nPin: origin nginx.org\nPin: release o=nginx\nPin-Priority: 900\n" | sudo tee /etc/apt/preferences.d/99nginx

  exit 0
else
  echo "fingerprint not matched"
fi

