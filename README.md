## Monitoring with Ansible
Prometheus/Alertmanager/Grafana set up managed by Ansible

SSL certificate integration

Sends firing alerts to Discord




## Screenshots

![grafana](https://gitlab.com/tbc-devops1/ansible_monitoring_tbc/-/wikis/uploads/ecd5208917756dc8dc98f07a5799ef39/4.png)

![alertmanager](https://gitlab.com/tbc-devops1/ansible_monitoring_tbc/-/wikis/uploads/4cf7c9714608f64f0e07ddfdc466a1f4/2.png)

![prometheus](https://gitlab.com/tbc-devops1/ansible_monitoring_tbc/-/wikis/uploads/7fbca1b3db8509e99b56cf883bb327b4/3.png)

![discord](https://gitlab.com/tbc-devops1/ansible_monitoring_tbc/-/wikis/uploads/4c8353f81076fceb09072b24244b1d53/1.png)

